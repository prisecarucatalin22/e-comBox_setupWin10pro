﻿# Initialisation de l'application. Script lancé à la fin du setup.

# Déclaration des chemins (logs, scripts, bibliothèque des fonctions et paramètres)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"


# Gestion des logs
If (-not (Test-Path "$env:USERPROFILE\.docker\logEcombox")) { New-Item -ItemType Directory -Path "$env:USERPROFILE\.docker\logEcombox" }

Write-host ""
Write-host "Création d'un fichier de log"
Write-host ""

If (Test-Path "$env:USERPROFILE\.docker\logEcombox\ecombox.log") {

$newName = "ecombox_$(Get-date -format 'ddMMyy_HHmmss').log"

Rename-Item -Path "$env:USERPROFILE\.docker\logEcombox\ecombox.log" -NewName $newName

# En cas de problème d'accès au fichier log
  If ($? -eq 0) {
    $allProcesses = Get-Process
    foreach ($process in $allProcesses) { 
      $process.Modules | where {$_.FileName -eq "$pathlog\ecombox.log"} | Stop-Process -Force -ErrorAction SilentlyContinue
    }
   Rename-Item -Path "$env:USERPROFILE\.docker\logEcombox\ecombox.log" -NewName $newName
  }
}

New-Item -Path "$pathlog\ecombox.log" -ItemType file -force

# 


Write-Output "=========================================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Initialisation de l'application" >> $pathlog\ecombox.log
Write-Output "=========================================================================" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log
Write-host "========================================================================="
Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Initialisation de l'application"
Write-host "========================================================================="


# Détection éventuelle d'une version 2
if (((docker ps -a) | Select-String e-combox:2.0)) {
   New-Item -Path "$pathconf\version2" -ItemType file -force
   If (Test-Path "$pathconf\.version_correctif") {Remove-Item -Path "$pathconf\.version_correctif" -Force}
   If (Test-Path "$pathconf\.version_correcti_gitlabf") {Remove-Item -Path "$pathconf\.version_correctif_gitlab" -Force}     
   }

# Ajout des informations importantes sur le matériel dans les logs
ajoutInfosLog

# Ajout du fichier de paramètres récupéré sur gitlab
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Ajout du fichier de paramètres" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log
If (-not (Test-Path "$env:USERPROFILE\.docker\confEcombox")) { New-Item -ItemType Directory -Path "$env:USERPROFILE\.docker\confEcombox" }
curl https://gitlab.com/e-combox/e-comBox_setupWin10pro/raw/$branche/param.conf -OutFile $pathconf\param.conf

# Vérification que Docker fonctionne correctement sinon on le redémarre
verifDocker
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de vérification de Docker." >> $pathlog\ecombox.log


# Ajout de infos sur le démarrage de Docker et WSL2 dans les log
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Docker est-il bien démarré sur la version 2 de WSL2" >> $pathlog\ecombox.log
wsl -l -v 2>&1 >> $pathlog\ecombox.log

# Authentification sur Docker (nécessaire dans certains cas de figure pour docker-compose)
docker login -u ecombox -p eCOMdock0 2>$null >> $pathlog\ecombox.log

# Création éventuelle du réseau 192.168.97.0/24 utilisé pour tous les conteneurs de l'appli
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Création du réseau des sites" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

if ((docker network ls) | Select-String bridge_e-combox) {
   Write-Output "Le réseau des sites existe déjà." >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   }
else {
   Write-Output "Le réseau des sites 192.168.97.0/24 n'existe pas, il sera créé :" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   docker network create --subnet 192.168.97.0/24 --gateway=192.168.97.1 bridge_e-combox *>> $pathlog\ecombox.log
}

# Récup automatique d'une adresse IP valide
$docker_ip_host = recupIPhost

# Mise à jour du fichier de paramètres
$ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_IP
(Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_IP=$docker_ip_host"} | Set-Content -Path $pathconf\param.conf
    

# Configuration du proxy sur Docker
configProxyDocker

# Détection et configuration d'un éventuel proxy pour Git
configProxyGit

# Suppression de l'application
deleteApplication


# Récupération, configuration et démarrage du reverse proxy
recupReverseProxy
configReverseProxy
startReverseProxy
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage du Reverse Proxy." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage du Reverse Proxy."

verifReverseProxy

# Démarrage du registry
startRegistry 

# Démarrage du serveur Git local 
startGitServer

# Nettoyage des anciennes images si elles ne sont associées à aucun site (désactivé temporairement pour la v3)
nettoyageImages
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images."

# Démarrage de l'application
startApplication

# Suppression éventuelle des variables d'environnement du Proxy sur Portainer en Supprimant le fichier config.json avant de démarrer portainer
supProxyPortainer

# Récupération de portainer 
recupPortainer
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus d'installation de Portainer." >> $pathlog\ecombox.log

configPortainer 
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de configuration de Portainer." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de configuration de Portainer."

# Démarrage de Portainer et verification de son bon fonctionnement
startPortainer
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage de Portainer." >> $pathlog\ecombox.log
Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de démarrage de Portainer."

#verifDocker
verifPortainer
#verifPortainerApresVerifDocker

# Configuration de l'application
configApplication

# Synchronisation éventuelle du mot de passe de Portainer
synchroPassPortainer

# Création du fichier .htaccess si besoin
creerAuth
 
if ((Test-Path "$pathconf\.version_correctif") -or (Test-Path "$pathconf\version2")) {
  
   # Lancement de la migration
   Write-host ""
   Write-host "============================================================="
   Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Migration des sites"
   Write-host "============================================================="
   Write-host ""

   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "=============================================================" >> $pathlog\ecombox.log
   Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Migration des sites" >> $pathlog\ecombox.log
   Write-Output "=============================================================" >> $pathlog\ecombox.log

   popupExclamation -titre "Migration des sites" -message "Vous disposez de la version 2 de l'e-comBox, le système va mettre à jour vos sites. `n`nAttention ! Selon le nombre de sites et la puissance de votre ordinateur, cela peut prendre du temps !"
    
   $update = updateStacks

   popupInformation -titre "Migration des sites" -message "La mise à jour des sites est terminée."    

   # Suppression du fichier témoin et des anciens fichiers 
   Remove-Item -Path "$pathconf/version2" -Force
    
   # Nettoyage des anciennes images de la version 2
   nettoyageImages
   Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images." >> $pathlog\ecombox.log
   Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin processus de nettoyage des images."
          
   } else {
         # Arrêt de tous les stacks avant de lancer l'application
         stopStacks
     }



#verifEspace
lanceURL
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin processus du démarrage et du lancement de l'application." >> $pathlog\ecombox.log