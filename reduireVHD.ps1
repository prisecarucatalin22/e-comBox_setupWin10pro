﻿# Optimisation du VHDX avec diskpart

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "===================================================================" >> $pathlog\ecombox.log
 Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Optimisation de l'espace" >> $pathlog\ecombox.log
 Write-Output "===================================================================" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log


# Suppression des images non utilisées
nettoyageImages

# Optimisation du VHDX
reductionVHD

