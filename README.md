# e-comBox_setupWin10pro

Vous trouverez ici tous les scripts utiles au SETUP permettant d'installer e-comBox sur Windows 10 Pro/Ent/Edu/Family.

Pour en savoir plus, rendez-vous sur le [wiki](https://llb.ac-corse.fr/mw/index.php/Installation_sur_Windows_-_v3) de l'e-comBox.
